package data.entities;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="MEDICO")
@PrimaryKeyJoinColumn(referencedColumnName = "id")
public class Medico extends Usuario{

    //Sin id puesto que es un Usuario.
    @Column(name="numColegiado")
    private String numColegiado;

    /*
    @OneToMany
    @JoinColumn(name="citas")
     */

    @OneToMany(mappedBy = "medico")
    private List<Cita> citas;






    //@ManyToMany(targetEntity = Paciente.class)
    //@Column(name="pacientes")

    @JoinTable(name="medico_paciente",
            joinColumns = {@JoinColumn(name="medico_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name="paciente_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Paciente> pacientes;





    public Medico(){
    }

    public Medico(String nombre, String apellidos, String usuario, String clave, String numColegiado) {
        super(nombre, apellidos, usuario, clave);
        this.numColegiado = numColegiado;
        this.citas = new LinkedList<Cita>();
        this.pacientes =new LinkedList<Paciente>();
    }

    //Getters and setters
    public String getNumColegiado() {
        return numColegiado;
    }
    public void setNumColegiado(String numColegiado) {
        this.numColegiado = numColegiado;
    }


    public List<Cita> getCitas() {
        return new LinkedList<Cita>(citas);
    }
    public void setCitas(List<Cita> citas) {
        this.citas = citas;
    }





    public List<Paciente> getPacientes() {
        return new LinkedList<Paciente>(pacientes);
    }
    public void setPaciente(List<Paciente> pacientes) {
        this.pacientes = pacientes;
    }



}
