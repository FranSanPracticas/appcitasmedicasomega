package data.DTOs;

import data.DTOs.Child.MedicoChildDTO;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class PacienteDTO {

    private int id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private String clave;



    private String NSS;
    private String numTarjeta;
    private String telefono;
    private String direccion;

    private List<CitaDTO> citas;
    private List<MedicoChildDTO> medicos;




    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }




    public String getNSS() {
        return NSS;
    }
    public void setNSS(String NSS) {
        this.NSS = NSS;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }
    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<CitaDTO> getCitas() {
        return new LinkedList<CitaDTO>(citas);
    }
    public void setCitas(List<CitaDTO> citas) {
        this.citas = citas;
    }


    public List<MedicoChildDTO> getMedicos() {
        return new LinkedList<MedicoChildDTO>(medicos);
    }
    public void setMedicos(List<MedicoChildDTO> medicos) {
        this.medicos = medicos;
    }



}

