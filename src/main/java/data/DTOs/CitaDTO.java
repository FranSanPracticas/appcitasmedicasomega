package data.DTOs;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import data.DTOs.Child.MedicoChildDTO;
import data.DTOs.Child.PacienteChildDTO;

import java.io.Serializable;
import java.util.Date;
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
//@JsonIdentityReference(alwaysAsId=true)
public class CitaDTO implements Serializable {

    private int id;
    private Date fechaHora;
    private String motivoCita;
    private PacienteChildDTO paciente;
    private MedicoChildDTO medico;
    //@JsonIgnore
    private DiagnosticoDTO diagnostico;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getMotivoCita() {
        return motivoCita;
    }

    public void setMotivoCita(String motivoCita) {
        this.motivoCita = motivoCita;
    }



    public PacienteChildDTO getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteChildDTO paciente) {
        this.paciente = paciente;
    }




    public MedicoChildDTO getMedico() {
        return medico;
    }

    public void setMedico(MedicoChildDTO medico) {
        this.medico = medico;
    }


    public DiagnosticoDTO getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(DiagnosticoDTO diagnostico) {
        this.diagnostico = diagnostico;
    }



}