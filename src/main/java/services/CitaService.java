package services;

import data.DTOs.CitaDTO;
import data.entities.Cita;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.CitaRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class CitaService implements ICitaService {

    @Autowired
    CitaRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private CitaDTO mapToDTO(Cita obj){
        CitaDTO dto = mapper.map(obj, CitaDTO.class);
        return dto;
    }

    private Cita mapToEntity(CitaDTO dto){
        Cita obj = mapper.map(dto, Cita.class);
        return obj;
    }

    @Override
    public List<CitaDTO> getAll() {
        Iterable<Cita> lista = repo.findAll();
        List<CitaDTO> listaDTO = new LinkedList<CitaDTO>();
        for (Cita cita: lista) {
            listaDTO.add(mapToDTO(cita));
        }
        return listaDTO;
    }

    @Override
    public CitaDTO getById(int id) {
        Optional<Cita> opt = repo.findById(id);
        return mapToDTO(opt.get());
    }
    @Override
    public boolean existsById(int id){
        return repo.existsById(id);
    }

    @Override
    public CitaDTO add(CitaDTO obj) {
        repo.save(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) {
        if(!repo.existsById(id)){
            System.out.println("La cita con id " + id + " no existe.");
        } else {
            repo.deleteById(id);
        }
    }

    @Override
    public void update(CitaDTO obj) {
        //TODO: Implementar
        repo.save(mapToEntity(obj));
    }
}

