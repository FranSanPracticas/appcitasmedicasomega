package services;

import data.DTOs.UsuarioDTO;

import java.util.List;

public interface IUsuarioService {

    public List<UsuarioDTO> getAll();
    public UsuarioDTO getById(int id);
    public UsuarioDTO add(UsuarioDTO obj);
    public void delete(int id);
    public void update(UsuarioDTO obj);
}