package controllers;

import data.DTOs.UsuarioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.UsuarioService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping("/usuarios")
    public List<UsuarioDTO> getAll() {
        return service.getAll();
    }

    @GetMapping("/usuarios/{id}")
    public UsuarioDTO getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping("/usuarios/add")
    public UsuarioDTO add(@RequestBody UsuarioDTO usuarioDTO) {
        return service.add(usuarioDTO);
    }

    @DeleteMapping("/usuarios/delete/{id}")
    public void delete(@PathVariable int id) {
        service.delete(id);
    }

    @PutMapping("/usuarios/update")
    public void update(@RequestBody UsuarioDTO usuarioDTO) {
        service.update(usuarioDTO);
    }


}