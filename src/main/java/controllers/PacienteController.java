package controllers;

import data.DTOs.PacienteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.PacienteService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class PacienteController {

    @Autowired
    PacienteService service;

    @GetMapping("/pacientes")
    public List<PacienteDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/pacientes/{id}")
    public PacienteDTO getById(@PathVariable int id){
        return service.getById(id);
    }

    @PostMapping("/pacientes/add")
    public PacienteDTO add(@RequestBody PacienteDTO pacienteDTO){
        return service.add(pacienteDTO);
    }

    @DeleteMapping("/pacientes/delete/{id}")
    public void delete(@PathVariable int id){
        service.delete(id);
    }

    @PutMapping("/pacientes/update")
    public void update(@RequestBody PacienteDTO pacienteDTO){
        service.update(pacienteDTO);
    }
}

