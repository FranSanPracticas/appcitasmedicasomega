package metaenlace.apps.AppCitasMedicasV7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableConfigurationProperties
@EnableJpaRepositories(basePackages = "repositories")
@EntityScan("data.entities")
@ComponentScan("controllers")
@ComponentScan("services")
@ComponentScan("security")
public class AppCitasMedicasV7Application {

	public static void main(String[] args) {
		SpringApplication.run(AppCitasMedicasV7Application.class, args);
	}

}
